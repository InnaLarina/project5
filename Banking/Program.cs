﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Banking
{
    class Program
    {
        static void Main(string[] args)
        {
            List<User> listUser = new List<User>();
            listUser.Add(new User(1,"Alisson","","","","",DateTime.Now,"Alisson","021092"));
            listUser.Add(new User(4, "Virgil", "Van Dijk", "", "", "", DateTime.Now, "Virgil", "080791"));
            listUser.Add(new User(32, "Joel", "Matip", "", "", "", DateTime.Now, "Joel", "080891"));
            listUser.Add(new User(6, "Dejan", "Lovren", "", "", "", DateTime.Now, "Dejan", "050689"));
            listUser.Add(new User(26, "Andrew Robertson", "", "", "", "", DateTime.Now, "Andrew", "110394"));
            listUser.Add(new User(66, "Trent", "Alexandr-Arnold", "", "", "", DateTime.Now, "Trent", "071098"));
            listUser.Add(new User(3, "Fabinho", "", "", "", "", DateTime.Now, "Fabinho", "231093"));

            List<Account> listAccount = new List<Account>();
            listAccount.Add(new Account(10, DateTime.Now, 40000, 1));
            listAccount.Add(new Account(11, DateTime.Now, 80800, 1));
            listAccount.Add(new Account(12, DateTime.Now, 500, 1));
            listAccount.Add(new Account(40, DateTime.Now, 1140000, 4));
            listAccount.Add(new Account(41, DateTime.Now, 51140, 4));
            listAccount.Add(new Account(30, DateTime.Now, 511400, 3));
            listAccount.Add(new Account(31, DateTime.Now, 88500, 3));

            List<History> listHistory = new List<History>();
            listHistory.Add(new History(10,DateTime.Now.AddDays(-89),OperationType.inflow,45000,10));
            listHistory.Add(new History(11, DateTime.Now.AddDays(-59), OperationType.outflow, 1500, 10));
            listHistory.Add(new History(12, DateTime.Now.AddDays(-39), OperationType.inflow, 101500, 10));
            listHistory.Add(new History(20, DateTime.Now.AddDays(-49), OperationType.inflow, 35000, 11));
            listHistory.Add(new History(21, DateTime.Now.AddDays(-39), OperationType.outflow, 500, 11));
            listHistory.Add(new History(22, DateTime.Now.AddDays(-29), OperationType.inflow, 18500, 11));
            //1
            Console.WriteLine("This is an information about the user Trent Alexandr-Arnold:");
            User userAccount = listUser.Where(u => u.Name == "Trent" && u.LastName == "Alexandr-Arnold").FirstOrDefault();
            Console.WriteLine(userAccount);
            Console.WriteLine();
            //2
            Console.WriteLine("These are all the accounts of the user Alisson:");
            IEnumerable <Account> accountsForUser = from account in listAccount
                                  join user in listUser on account.IdUser equals user.Id
                                  where user.Name == "Alisson"
                                  select account;
            foreach (var au in accountsForUser)
            {
                Console.WriteLine(au.ToString());
            }
            Console.WriteLine();
            //3
            Console.WriteLine("These are the Alisson' accounts with theirs history:");
            var historyUser = from history in listHistory
                              join account in listAccount on history.IdAccount equals account.Id
                              join user in listUser on account.IdUser equals user.Id
                              where user.Name == "Alisson"
                              orderby account.Id, history.OperDate
                              select new { User = user.ToString(), Account = account.ToString(), Transaction = history.ToString() };

            string old_account = "";
            String repeatedString = new String(' ', 45);
            Console.WriteLine("Alisson TheFootballPlayer account:");
            foreach (var hu in historyUser)
            {
                if (old_account != hu.Account)
                    Console.WriteLine(hu.Account + " " + hu.Transaction);
                else Console.WriteLine(repeatedString + " " + hu.Transaction);
                old_account = hu.Account;
            }
            Console.WriteLine();
            //4
            Console.WriteLine("These are inflow operations:");
            var historyInflow = from history in listHistory
                                join account in listAccount on history.IdAccount equals account.Id
                                join user in listUser on account.IdUser equals user.Id
                                where history.Operartion == OperationType.inflow
                                orderby history.OperDate 
                                select new { Transaction = history.ToString(), User = user.ToString() };
            foreach (var hi in historyInflow)
            {
                Console.WriteLine(hi.Transaction+" "+hi.User);
            }
            Console.WriteLine();
            //5
            try
            {
                Console.Write("Input amount of income and press Enter, please: ");
                string sumString = Console.ReadLine();
                int sumAccount = Convert.ToInt32(sumString);
                Console.WriteLine($"These happy guys have more than {sumString} euro in theirs accounts.  ");
                IEnumerable<User> userAccounts = from user in listUser
                                                 join account in listAccount on user.Id equals account.IdUser
                                                 where account.sum > sumAccount
                                                 select user;
                foreach (User user in userAccounts)
                {
                    Console.WriteLine(user);
                }
                if(!userAccounts.Any()) Console.WriteLine("There is none");
            }
            catch (Exception ex) 
            {
                Console.WriteLine("Неверно введенные данные");
            }                                             
        }
    }
}
